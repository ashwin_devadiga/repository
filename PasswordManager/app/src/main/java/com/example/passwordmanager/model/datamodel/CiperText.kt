package com.example.passwordmanager.model.datamodel

data class CipherText(val cipherText: ByteArray, val iv: ByteArray, val key: String)
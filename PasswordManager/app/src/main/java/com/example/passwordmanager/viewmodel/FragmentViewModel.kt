package com.example.passwordmanager.viewmodel

import android.os.Looper
import android.os.Message
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.example.passwordmanager.helpers.LoginInterface
import com.example.passwordmanager.model.db.PasswordManagerDatabase
import com.example.passwordmanager.model.db.User
import com.example.passwordmanager.model.repositories.UserRepository
import com.example.passwordmanager.utils.Coroutines
import com.example.passwordmanager.utils.isPasswordValid
import com.example.passwordmanager.utils.isPhoneValid
import java.lang.Exception
import java.util.logging.Handler
import java.util.logging.LogRecord


class FragmentViewModel(
   private val repository: UserRepository,
    private val db: PasswordManagerDatabase
) : ViewModel(){

    var mobileNumber : String? = null
    var mPin : String? = null
    var _mobileNumber : String? = null
    var _mPin : String? = null
    var _password : String? = null
    var loginInterface: LoginInterface? = null




    fun onClickSignIn(){

        if(mobileNumber.isNullOrEmpty()){
            loginInterface?.onInputValidation(1,"Enter Phone")
            return
        }
        if(!isPhoneValid(mobileNumber.toString())){
            loginInterface?.onInputValidation(1,"Enter Valid Phone number")
            return
        }
        if(mPin.isNullOrEmpty()) {
            loginInterface?.onInputValidation(2,"Enter Password")
            return
        }
        if (!isPasswordValid(mPin.toString())) {
            loginInterface?.onInputValidation(2, "Enter Valid Password")
            return
        }


        Coroutines.io {

            try {

                // db.getUserDao().deleteUser()
                val listUser = db.getUserDao().getUser()



                Log.d("number", "${listUser}")
                Log.d("numer3", "${mobileNumber},${mPin}")
                val i = listUser.size

                for (n in 0 until i) {
                    if ((listUser[n].mobileNumber == mobileNumber) && (listUser[n].mPin == mPin)) {
                        loginInterface?.onSuccess()
                        return@io
                    } else {
                        // loginInterface?.onFailure("Enter Valid number or password")
                        Log.d("hello", "Invalid phone number or password")

                    }
                }
            }catch (e: Exception)
            {
               // loginInterface?.onFailure("Enter Valid number or password")
            }
        }
    }


    fun onClickSignUp(){

        if(_mobileNumber.isNullOrEmpty()){
            loginInterface?.onInputValidation(1,"Enter Phone")
            return
        }

        if(!isPhoneValid(_mobileNumber.toString())){
           loginInterface?.onInputValidation(1,"Enter Valid Phone number")
            return
        }


        if(_mPin.isNullOrEmpty() && _password.isNullOrEmpty()) {
            loginInterface?.onInputValidation(2,"Enter Password")
            return
        }
        if(_mPin == _password) {
            if (!isPasswordValid(_password.toString(), _mPin.toString())) {
               loginInterface?.onInputValidation(2, "Enter Valid Password")
                return
            }
        }else {
            loginInterface?.onInputValidation(2, "Enter Same Password")
        }


      Coroutines.io {
         // db.getUserDao().deleteUser()
         // db.getDao().deleteSites()
          // Log.d("Print2", "${_mobileNumber},${_mPin},${_password}")
          db.getUserDao().onSignUp(User(_mobileNumber.toString(), _password.toString()))
          Log.d("print", "${db.getUserDao().getUser()}")
      }
          loginInterface?.onSignup()

    }

    fun  onClickForgotPassword(){

    }


}

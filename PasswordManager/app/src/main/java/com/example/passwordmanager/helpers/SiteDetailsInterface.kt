package com.example.passwordmanager.helpers

interface SiteDetailsInterface {
    fun onSave()
    fun onUpdate()
    val userId: String?
}
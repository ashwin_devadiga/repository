package com.example.passwordmanager.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_data")
data class User(
    @PrimaryKey(autoGenerate = false)
    val mobileNumber:String,

    val mPin: String

) {


}
package com.example.passwordmanager.ui

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.passwordmanager.R
import com.example.passwordmanager.fragment.SigninFragment
import com.example.passwordmanager.fragment.SignupFragment
import com.example.passwordmanager.model.db.PasswordManagerDatabase
import kotlinx.android.synthetic.main.activity_login_screen.*


class LoginActivity : AppCompatActivity(){

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_screen)
        supportActionBar?.hide()
        var i: Int? =null
       // val userid: String = resources.getString(R.string.user_id)



        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frameLayout, SigninFragment(i))
        fragmentTransaction.commit()


       // val db = PasswordManagerDatabase(this, userid)

        val bundle: Bundle? = intent.extras

         i = bundle?.getInt("signup")
       if(i == 1){
           loadFragment(SigninFragment(i))

        }

        val signIn: TextView= findViewById(R.id.tvSignIn)
        val signUp: TextView= findViewById(R.id.tvSignUp)


        signIn.setOnClickListener {
            viewSignup.visibility = View.INVISIBLE
            viewSignin.visibility = View.VISIBLE
            signIn.setTextAppearance(R.style.OnClick)
            signUp.setTextAppearance(R.style.SignUp)
            loadFragment(SigninFragment(i))

        }

        signUp.setOnClickListener {
            viewSignup.visibility = View.VISIBLE
            viewSignin.visibility = View.INVISIBLE
            signUp.setTextAppearance(R.style.OnClick)
            signIn.setTextAppearance(R.style.SignUp)
            loadFragment(SignupFragment())

        }


    }

    fun loadFragment(fragment: Fragment){
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frameLayout, fragment )
        fragmentTransaction.commit()

    }
}

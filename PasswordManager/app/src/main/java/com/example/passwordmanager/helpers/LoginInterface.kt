package com.example.passwordmanager.helpers

interface LoginInterface {

    fun onSuccess()
    fun onFailure(s: String)
    fun onInputValidation(field: Int, message: String)
    fun onSignup()

}
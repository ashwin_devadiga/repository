package com.example.passwordmanager.model.db

import androidx.room.*
import androidx.room.Dao

@Dao
interface UserDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun onSignUp(user: User)

    @Query("SELECT * FROM user_data")
    fun getUser(): List<User>

    @Query("DELETE FROM user_data")
    fun deleteUser()

}
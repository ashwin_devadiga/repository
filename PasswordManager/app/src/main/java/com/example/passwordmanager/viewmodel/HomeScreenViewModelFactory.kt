package com.example.passwordmanager.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.passwordmanager.model.db.PasswordManagerDatabase
import com.example.passwordmanager.model.repositories.UserRepository

class HomeScreenViewModelFactory(private val repository: UserRepository,
                                 private val db: PasswordManagerDatabase
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeScreenViewModel(repository, db) as T
    }
}
package com.example.passwordmanager.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.passwordmanager.R
import com.example.passwordmanager.databinding.ActivitySitedetailsBinding
import com.example.passwordmanager.helpers.SiteDetailsInterface
import com.example.passwordmanager.helpers.decrypt
import com.example.passwordmanager.model.db.PasswordManagerDatabase
import com.example.passwordmanager.model.db.SitesData
import com.example.passwordmanager.model.repositories.UserRepository
import com.example.passwordmanager.viewmodel.SiteDetailsViewModel
import com.example.passwordmanager.viewmodel.SiteDetailsViewModelFactory
import kotlinx.android.synthetic.main.activity_sitedetails.*
import kotlinx.android.synthetic.main.customtoast.*
import kotlinx.android.synthetic.main.customtoast.view.*
import java.lang.Exception


class SiteDetailsActivity : AppCompatActivity(), SiteDetailsInterface, AdapterView.OnItemSelectedListener {
    val categories = ArrayList<String>()
    var temp: Int = 0
   override var userId: String? = null
    val dataAdapter: ArrayAdapter<String>? = null
    var layout: View? = null

    var edit: MenuItem? = null
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_site_details)
        val userid: String = resources.getString(R.string.user_id)

        val bundle: Bundle? = intent.extras
        temp = bundle?.getInt("AddSite")!!
        userId = bundle?.getString("userid")


        val db = PasswordManagerDatabase(this,userid)
        val repository = UserRepository(db)
        val factory = SiteDetailsViewModelFactory(repository, db)
        val binding: ActivitySitedetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_sitedetails)
        val viewModel = ViewModelProviders.of(this, factory).get(SiteDetailsViewModel::class.java)
        viewModel.siteDetailsInterface = this
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        layout = layoutInflater.inflate(R.layout.toast_layout, findViewById(R.id.toastLinearLayout))

        categories.add("")
        categories.add("Social Media")
        categories.add("Shopping")


        spinner.setOnItemSelectedListener(this)
        val dataAdapter: ArrayAdapter<String> =  ArrayAdapter(this, android.R.layout.simple_spinner_item, categories)

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)


        //action bar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#0E85FF")))



        if(temp == 0){

            updateButton.visibility = View.INVISIBLE
            supportActionBar?.setTitle("Add Site")
            spinner.setAdapter(dataAdapter)


        }else {
            saveReset.visibility = View.INVISIBLE
            updateButton.visibility = View.INVISIBLE
            var site: String? = bundle.getString("sitename")
           // var id: Int = bundle?.getInt("id")
            supportActionBar?.setTitle("Site Details")
            getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            )
            if (site != null) {
                viewModel.getSiteDetails(site)
                viewModel.sites!!.observe(this, Observer<SitesData> {
                    try {

                        viewModel.url.value = it.url
                        // etFolder.setText(it.folder)
                        viewModel.siteName.value = it.siteName
                        viewModel.folder.value = it.folder
                        viewModel.sitePassword.value = decrypt(it.cipherText, it.iv!!, it.key!!)
                        if (it.notes != null) {
                            //  viewModel.notes.value = it.notes
                        }
                        viewModel.userName.value = it.userName
                        site = null
                    }
                    catch (e:Exception){
                        return@Observer
                    }
                })

            }
           // }
        }


    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onSave() {
        val toast = Toast(this)
        toast.duration = Toast.LENGTH_SHORT
        toast.setView(layout) //setting the view of custom toast layout
        toast.show()
        val intent = Intent(this, HomeScreenActivity::class.java)
        intent.putExtra("userid", userId)
        startActivity(intent)
    }

    override fun onUpdate() {
        startActivity(Intent(this, HomeScreenActivity::class.java))
        return

    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        val item: String = categories[p2]
       etFolder.setText(item)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: Unit
        if(temp == 1) {
            val item = menu?.findItem(R.menu.menu)

            menuInflater.inflate(R.menu.menu, menu)
            item?.isVisible = true

        }
        return super.onCreateOptionsMenu(menu )
    }

    @SuppressLint("RestrictedApi")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.edit -> {
                supportActionBar?.setTitle("Edit")
               updateButton.visibility = View.VISIBLE
               item.setVisible(false)
                spinner.setAdapter(dataAdapter)


            }
        }
        return super.onOptionsItemSelected(item)
    }





}

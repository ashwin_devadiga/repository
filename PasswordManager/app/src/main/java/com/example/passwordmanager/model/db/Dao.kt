package com.example.passwordmanager.model.db

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.Dao

@Dao
interface Dao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveSiteDetail(userSites: SitesData)

    @Query("SELECT * FROM site_Table WHERE userId = :userId")
    fun getSiteDetails(userId: String?): LiveData<List<SitesData>>


    @Query("SELECT * FROM site_Table WHERE siteName = :site")
    fun getSiteDetail(site: String): LiveData<SitesData>

    @Query("SELECT * FROM site_Table WHERE folder = :S AND userId = :userId")
    fun getSite(S: String, userId: String?): LiveData<List<SitesData>>

    @Query("DELETE FROM site_Table WHERE url = :url")
    fun deleteSite(url: String)

    @Query("DELETE FROM site_Table")
    fun deleteSites()

}
package com.example.passwordmanager.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.passwordmanager.R
import com.example.passwordmanager.databinding.FragmentSigninBinding
import com.example.passwordmanager.helpers.LoginInterface
import com.example.passwordmanager.model.db.PasswordManagerDatabase
import com.example.passwordmanager.model.repositories.UserRepository
import com.example.passwordmanager.ui.HomeScreenActivity
import com.example.passwordmanager.viewmodel.FragmentViewModel
import com.example.passwordmanager.viewmodel.LoginViewModelFactory
import kotlinx.android.synthetic.main.fragment_signin.*


class SigninFragment(val i: Int?) : Fragment(), LoginInterface {
    private lateinit var factoty: LoginViewModelFactory
    private lateinit var viewModel:  FragmentViewModel
    var binding:FragmentSigninBinding? = null
    val loginListener : LoginInterface? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val userid: String = "login"
        val db = PasswordManagerDatabase(requireContext().applicationContext, userid)
        val repository = UserRepository(db)
        factoty = LoginViewModelFactory(repository, db)

        // TODO: Use the ViewModel
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_signin, container, false)
        viewModel = ViewModelProviders.of(this, factoty).get(FragmentViewModel::class.java)
        binding!!.viewmodel = viewModel
        viewModel.loginInterface = this
        val view: View = binding!!.root
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if(i == 1){
            textViewOr.visibility =View.VISIBLE
            textView.visibility = View.INVISIBLE
            textView2.visibility = View.INVISIBLE
        }

    }

    override fun onSuccess() {
        val intent = Intent(requireContext(), HomeScreenActivity::class.java)
        intent.putExtra("userid",etMobileNumber.text.toString())
        startActivity(intent)
    }

    override fun onFailure(s: String) {

          //  Toast.makeText(requireContext(), s,Toast.LENGTH_LONG).show()

    }

    override fun onInputValidation(field: Int, message: String) {
        if (field == 1) {
            binding!!.etMobileNumber.error = message
            binding!!.etMobileNumber.requestFocus()
        }
        if (field == 2) {
            binding!!.etmPin.error = message
            binding!!.etmPin.requestFocus()
        }
    }

    override fun onSignup() {

    }
}

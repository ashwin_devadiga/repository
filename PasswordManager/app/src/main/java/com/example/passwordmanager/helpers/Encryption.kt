package com.example.passwordmanager.helpers


import android.os.Build
import android.util.Log
import com.example.passwordmanager.model.datamodel.CipherText
import java.security.SecureRandom
import java.util.*
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec




private const val ALGORITHM = "AES"
private const val TRANSFORMATION = "AES/CBC/PKCS5PADDING"

fun encrypt(s : String): CipherText {
    val charset = Charsets.UTF_8
    val plaintext: ByteArray = s.toByteArray(charset)
    val keygen = KeyGenerator.getInstance(ALGORITHM)
    keygen.init(256)
    val key = keygen.generateKey()
    val encodedKey: String = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        Base64.getEncoder().encodeToString(key.encoded)
    } else {
        android.util.Base64.encodeToString(key.encoded, android.util.Base64.DEFAULT);
    }
    val cipher = Cipher.getInstance(TRANSFORMATION)
    cipher.init(Cipher.ENCRYPT_MODE, key)
    val ciphertext: ByteArray = cipher.doFinal(plaintext)
    val iv = cipher.iv
    return CipherText(ciphertext, iv, encodedKey)

}

fun decrypt(ciphertext: ByteArray, ivToDecrypt: ByteArray, encodedKey: String):String {
    val charset = Charsets.UTF_8
    val encryptedText: ByteArray = ciphertext
    val keygen = KeyGenerator.getInstance(ALGORITHM)
    keygen.init(256)
    val cipher = Cipher.getInstance(TRANSFORMATION)
    val decodedKey = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        Base64.getDecoder().decode(encodedKey)
    } else {

        android.util.Base64.decode(encodedKey, android.util.Base64.DEFAULT)
    }
    // rebuild key using SecretKeySpec
    val originalKey: SecretKey = SecretKeySpec(decodedKey, 0, decodedKey.size, ALGORITHM)
    val iv = IvParameterSpec(ivToDecrypt)
    cipher.init(Cipher.DECRYPT_MODE, originalKey, iv)
    val original: ByteArray = cipher.doFinal(encryptedText)

    Log.d("original password", original.toString(charset))
    return original.toString(charset)
}




//Type a message




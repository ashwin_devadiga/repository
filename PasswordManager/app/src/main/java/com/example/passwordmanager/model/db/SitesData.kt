package com.example.passwordmanager.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "site_Table")
data class SitesData(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val url: String,
    val siteName: String,
    val folder: String,
    val userName: String,
    val sitePassword: ByteArray,
    val notes:String? = null,
    val cipherText: ByteArray,
    var iv: ByteArray? = null,
    var key: String? = null,
    val userId: String
)


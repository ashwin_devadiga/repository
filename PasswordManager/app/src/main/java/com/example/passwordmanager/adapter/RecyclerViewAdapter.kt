package com.example.passwordmanager.adapter

import android.content.ClipData
import android.content.ClipboardManager

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.passwordmanager.R
import com.example.passwordmanager.helpers.decrypt
import com.example.passwordmanager.model.db.SitesData
import com.example.passwordmanager.ui.HomeScreenActivity
import com.example.passwordmanager.ui.SiteDetailsActivity
import kotlinx.android.synthetic.main.sitedetails_cardview.view.*


class RecyclerViewAdapter(
    var siteDetails: ArrayList<SitesData>,
    val context: HomeScreenActivity,
   val userId: String?
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        init {
            val etUrl = itemView.siteUrl
            val  etSiteNmae = itemView.siteName
            val cardView = itemView.findViewById<CardView>(R.id.cardview)
        }

      fun onBind(position: Int) {


        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerViewAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.sitedetails_cardview, parent, false)
        return  ViewHolder(v)

    }

    override fun getItemCount(): Int {
       return  siteDetails.size
    }

    override fun onBindViewHolder(holder: RecyclerViewAdapter.ViewHolder, position: Int) {
        holder.itemView.siteUrl.text = siteDetails[position].url
        holder.itemView.siteName.text = siteDetails[position].siteName


        holder.itemView.cardview.setOnClickListener {

            val intent = Intent(context, SiteDetailsActivity::class.java)
            intent.putExtra("AddSite", 1)
            intent.putExtra("userid", userId)
           // Log.d("sqqqq","${siteDetails[position].siteName}")
            intent.putExtra("sitename", "${siteDetails[position].siteName}")
           // intent.putExtra("id", siteDetails[position].id)
            context.startActivity(intent)

        }

        holder.itemView.siteUrl.setOnClickListener {



            val intent = Intent(Intent.ACTION_VIEW)
            val url = holder.itemView.siteUrl.text.toString()
            if(url.startsWith("https://") ){
                intent.data = Uri.parse("${url}")

            }else {
                intent.data = Uri.parse("https://${url}")
                // holder.itemView.siteUrl.text.toString()
                Toast.makeText(context, "thanks", Toast.LENGTH_LONG).show()
            }
            context.startActivity(intent)

        }
        holder.itemView.copyPassword.setOnClickListener {
            val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip: ClipData = ClipData.newPlainText("copy", "${decrypt(siteDetails[position].cipherText,siteDetails[position].iv!!,siteDetails[position].key!!)}");
            clipboard?.setPrimaryClip(clip)
        }

    }


    fun filterList(filterdNames: ArrayList<SitesData>) {
        this.siteDetails = filterdNames
        notifyDataSetChanged()
        return
    }



}
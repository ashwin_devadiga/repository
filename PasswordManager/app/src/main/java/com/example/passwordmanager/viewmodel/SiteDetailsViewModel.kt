package com.example.passwordmanager.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.passwordmanager.helpers.*
import com.example.passwordmanager.model.datamodel.CipherText
import com.example.passwordmanager.model.db.PasswordManagerDatabase
import com.example.passwordmanager.model.db.SitesData
import com.example.passwordmanager.model.repositories.UserRepository
import com.example.passwordmanager.utils.Coroutines
import kotlinx.coroutines.launch

class SiteDetailsViewModel(
    private val repository: UserRepository,
    private  val db: PasswordManagerDatabase
) : ViewModel() {

    val url = MutableLiveData<String>()
    val siteName = MutableLiveData<String>()
    val folder = MutableLiveData<String>()
    val userName = MutableLiveData<String>()
    val sitePassword = MutableLiveData<String>()
    val notes = MutableLiveData<String>()

    var id: Int? = null



    var sites: LiveData<SitesData>? = null

    var siteDetailsInterface: SiteDetailsInterface? = null


    fun onUpdate(

    ) {
        val encryption = encrypt(sitePassword.value!!)
        val cipher : CipherText? = null
        Log.d("hhhhhh","${encryption}")
        Log.d("bbb", sites!!.value!!.url.toString())
        val site = SitesData(
            sites!!.value?.id,
            url.value.toString(),
            siteName.value.toString(),
            folder.value.toString(),
            userName.value.toString(),
            encryption.cipherText,
            notes.value.toString(),
            encryption.cipherText,
            encryption.iv,
            encryption.key.toString(),
            siteDetailsInterface?.userId!!

        )
        Log.d("hhhhhh","${site}")
        Coroutines.io {
            db.getDao().saveSiteDetail(site)
            // val s =  db.getDao().getSiteDetails()

            // decrypt(s[0].cipherText, s[0].iv!!, s[0].key!!)
            // Log.d("site", "${db.getDao().getSiteDetails()}")

        }
        siteDetailsInterface?.onSave()
    }

    fun onReset() {
        url.value = ""
        siteName.value = ""
        sitePassword.value = ""
        userName.value = ""
        folder.value = ""
        notes.value = ""
    }

   fun onSave() {
         val encryption = encrypt(sitePassword.value!!)
       val cipher : CipherText? = null
       // Log.d("hhhhhh","${encryption}")
      // Log.d("bbb", sites!!.value!!.url.toString())
        val site = SitesData(
            id,
            url.value.toString(),
            siteName.value.toString(),
            folder.value.toString(),
            userName.value.toString(),
            encryption.cipherText,
            notes.value.toString(),
            encryption.cipherText,
            encryption.iv,
            encryption.key.toString(),
            siteDetailsInterface?.userId!!

        )
       Log.d("hhhhhh","${site}")
        Coroutines.io {
            db.getDao().saveSiteDetail(site)

        }
        siteDetailsInterface?.onSave()


    }

    fun getSiteDetails(site: String?) : Int{
       // var _site: UserSites? = null
        viewModelScope.launch {
           sites =  repository.get(site!!)
            Log.d("sites", sites?.value.toString())

        }
        return  1

    }

    fun decryption(){
     sitePassword.value = decrypt(sites!!.value!!.cipherText,sites!!.value!!.iv!!,sites!!.value!!.key!!)

    }


}
package com.example.passwordmanager.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.passwordmanager.model.db.PasswordManagerDatabase
import com.example.passwordmanager.model.repositories.UserRepository

class SiteDetailsViewModelFactory(private val repository: UserRepository,
                                  private val db: PasswordManagerDatabase
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SiteDetailsViewModel(repository, db) as T
    }
}
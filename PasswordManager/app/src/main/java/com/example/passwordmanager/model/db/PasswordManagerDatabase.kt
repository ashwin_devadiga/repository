package com.example.passwordmanager.model.db

import android.content.Context
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [User::class,  SitesData::class],
    version = 1,
    exportSchema = false
)


abstract  class PasswordManagerDatabase: RoomDatabase() {
    abstract fun getUserDao(): UserDao
   // abstract fun getSiteDao(): SiteDao
    abstract fun getDao(): com.example.passwordmanager.model.db.Dao


    companion object {

        @Volatile
        private var instance: PasswordManagerDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context, userid: String) = instance ?: synchronized(this) {
            instance ?: buildDatabase(context).also {
                instance = it

            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder<PasswordManagerDatabase>(
                context.applicationContext,
                PasswordManagerDatabase::class.java,
                "AppDatabase.db"
            ).build()
    }
}
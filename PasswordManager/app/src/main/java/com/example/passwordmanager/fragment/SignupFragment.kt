package com.example.passwordmanager.fragment


import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.passwordmanager.R
import com.example.passwordmanager.databinding.FragmentSignupBinding
import com.example.passwordmanager.helpers.LoginInterface
import com.example.passwordmanager.model.db.PasswordManagerDatabase
import com.example.passwordmanager.model.repositories.UserRepository
import com.example.passwordmanager.ui.LoginActivity
import com.example.passwordmanager.viewmodel.FragmentViewModel
import com.example.passwordmanager.viewmodel.LoginViewModelFactory


/**
 * A simple [Fragment] subclass.
 */
class SignupFragment() : Fragment(), LoginInterface {

    var layout: View? = null
   private lateinit var factoty: LoginViewModelFactory
    private  lateinit var viewModel : FragmentViewModel
     var binding: FragmentSignupBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val userid: String = "login"


        layout = layoutInflater.inflate(R.layout.customtoast, container?.findViewById(R.id.customToast))
         binding= DataBindingUtil.inflate(inflater, R.layout.fragment_signup, container, false)

        val db = PasswordManagerDatabase(requireContext().applicationContext, userid)
        val repository = UserRepository(db)
        factoty = LoginViewModelFactory(repository, db)
        viewModel = ViewModelProviders.of(this, factoty).get(FragmentViewModel::class.java)
        binding!!.viewmodel = viewModel
        viewModel.loginInterface = this
        val view: View = binding!!.root
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }

    override fun onSuccess() {

    }

    override fun onFailure(s: String) {

    }

    override fun onInputValidation(field: Int, message: String) {

        if (field == 1) {
            binding!!.etEnterMobileNumber.error = message
            binding!!.etEnterMobileNumber.requestFocus()
        }
        if (field == 2) {
            binding!!.etEnterPassword.error = message
            binding!!.etEnterPassword.requestFocus()
        }

    }

    override fun onSignup() {

        val toast = Toast(requireContext())
        toast.duration = Toast.LENGTH_SHORT
        toast.setView(layout) //setting the view of custom toast layout
        toast.show()

        val intent = Intent(requireContext(), LoginActivity::class.java)
        intent.putExtra("signup", 1)
        startActivity(intent)
    }


}


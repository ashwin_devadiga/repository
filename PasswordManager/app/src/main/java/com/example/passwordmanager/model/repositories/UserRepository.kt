package com.example.passwordmanager.model.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.passwordmanager.model.db.PasswordManagerDatabase
import com.example.passwordmanager.model.db.SitesData
import com.example.passwordmanager.model.db.User

class UserRepository(
    private  val db: PasswordManagerDatabase
) {


suspend fun getsiteDeatails(site: String, userId: String?): LiveData<List<SitesData>> {
    Log.d("ddddccc", userId!!)
      val sites =   db.getDao().getSite(site, userId)
    Log.d("dddd", sites.toString())
    return  sites
}

    suspend fun getDeatail(userId: String?): LiveData<List<SitesData>> {

            val sites = db.getDao().getSiteDetails(userId)

 return sites
}


    fun get(site: String): LiveData<SitesData>{
        val site = db.getDao().getSiteDetail(site)
        return site
    }

    suspend fun saveDetails(userSites: SitesData){



    }

    suspend fun onSignup(mobileNumber:String, password: String){
        db.getUserDao().deleteUser()
        //db.getDao().deleteSites()
        // Log.d("Print2", "${_mobileNumber},${_mPin},${_password}")
        db.getUserDao().onSignUp(User(mobileNumber, password))
    }

    suspend fun update(){

    }

    suspend fun delete(url: String){
        db.getDao().deleteSite(url)

    }
}
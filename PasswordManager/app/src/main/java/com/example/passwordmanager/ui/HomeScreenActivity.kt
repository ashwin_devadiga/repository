package com.example.passwordmanager.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.passwordmanager.R
import com.example.passwordmanager.adapter.RecyclerViewAdapter
import com.example.passwordmanager.databinding.ActivityHomescreenBinding
import com.example.passwordmanager.model.db.PasswordManagerDatabase
import com.example.passwordmanager.model.db.SitesData
import com.example.passwordmanager.model.repositories.UserRepository
import com.example.passwordmanager.viewmodel.HomeScreenViewModel
import com.example.passwordmanager.viewmodel.HomeScreenViewModelFactory
import kotlinx.android.synthetic.main.activity_homescreen.*


class HomeScreenActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    val categories = ArrayList<String>()
    var siteDetails=  ArrayList<SitesData>()
    lateinit var recyclerView: RecyclerView
    var folder:String? = null
    var adapter: RecyclerViewAdapter? = null
    var db:PasswordManagerDatabase? = null
    var repository: UserRepository? = null
    var viewModel: HomeScreenViewModel? = null
    var userId: String? = null

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_homescreen)
        val userid: String = resources.getString(R.string.user_id)



        val bundle: Bundle? = intent.extras
        userId = bundle?.getString("userid")
        Log.d("bundle", userId.toString())


        db = PasswordManagerDatabase(this, userid)
        repository = UserRepository(db!!)
        val factory = HomeScreenViewModelFactory(repository!!,db!!)
        val binding: ActivityHomescreenBinding = DataBindingUtil.setContentView(this, R.layout.activity_homescreen)
         viewModel = ViewModelProviders.of(this, factory).get(HomeScreenViewModel::class.java)



        menu_profile.visibility = View.INVISIBLE
        searchView.visibility = View.INVISIBLE
        recyclerView = findViewById(R.id.recyclerView)
        categories.add("Social Media")
        categories.add("Shopping")

        dropdownButton.setOnItemSelectedListener(this)
        val dataAdapter: ArrayAdapter<String> =  ArrayAdapter(this, android.R.layout.simple_spinner_item, categories)

        val toolbar: Toolbar = findViewById(R.id.toolBar)
        toolbar.setTitle("")
        setSupportActionBar(toolbar)

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        dropdownButton.setAdapter(dataAdapter)

        folder = tvFolder.text.toString()

        recyclerView.layoutManager = LinearLayoutManager(this@HomeScreenActivity)
        adapter = RecyclerViewAdapter(siteDetails, this, userId)
        recyclerView.adapter = adapter



        burgerMenu.setOnClickListener {
            searchView.visibility =View.INVISIBLE
            linearLayout.visibility = View.VISIBLE
            menu_profile.visibility = View.INVISIBLE
            dropdownButton.setAdapter(dataAdapter)
            viewModel!!.getUserDetails("Social Media",userId!!)
            viewModel!!.sites?.observe(this@HomeScreenActivity, Observer { items ->
                val size = items.size
                adapter!!.filterList(items as ArrayList<SitesData>)
                tvCount.text = size.toString()
            })

        }




       // adapter!!.filterList(siteDetails)


      tvFolder.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

                Log.d("textchange", p0.toString())
               viewModel!!.getUserDetails(p0.toString(), userId!!)

                viewModel!!.sites?.observe(this@HomeScreenActivity, Observer {
                        items ->
                    val size = items.size
                    adapter!!.filterList(items as ArrayList<SitesData>)
                    tvCount.text = size.toString()
                    Log.d("sdapter", items.toString())

                })


            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })


        searchView.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                filter(p0.toString())

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })




        addButton.setOnClickListener {
            val intent = Intent(this, SiteDetailsActivity::class.java)
            intent.putExtra("AddSite", 0)
            intent.putExtra("userid", userId)
            startActivity(intent)
        }

        viewModel!!.sites?.observe(this, Observer {
                items ->
            adapter!!.filterList(items as ArrayList<SitesData>)
            Log.d("sdapter", items.toString())

        })


        signOut.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.passmanager_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.search -> {
                searchView.visibility =View.VISIBLE
                linearLayout.visibility = View.INVISIBLE
                if(menu_profile.visibility == View.VISIBLE){
                    menu_profile.visibility = View.INVISIBLE
                }

            }
            R.id.cloudSync ->{}
            R.id.profile -> {
                menu_profile.visibility = View.VISIBLE
                linearLayout.visibility = View.VISIBLE
                if(searchView.visibility == View.VISIBLE){
                    searchView.visibility = View.INVISIBLE
                }

            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

        val item: String = categories[p2]
        tvFolder.text = item


    }

   fun filter(text: String) {

       Log.d("filter", text)
        val filterdNames: ArrayList<SitesData> = ArrayList()
       viewModel?.getUserDetail(userId!!)
       viewModel?.sites?.observe(this, Observer {
           siteDetail ->
           for (s in siteDetail) {
               Log.d("site", siteDetail.toString())
               if (s.siteName.toLowerCase().contains(text.toLowerCase())) {
                   filterdNames.add(s)
               }
           }
       })

        adapter!!.filterList(filterdNames)
    }

}

package com.example.passwordmanager.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.passwordmanager.model.db.PasswordManagerDatabase
import com.example.passwordmanager.model.db.SitesData
import com.example.passwordmanager.model.repositories.UserRepository
import kotlinx.coroutines.launch

class HomeScreenViewModel(
    private val repository: UserRepository,
    private val db: PasswordManagerDatabase

): ViewModel() {
    var sites: LiveData<List<SitesData>>? = null


    val folder: String? = null


    fun getUserDetails(site: String, userId: String){
        Log.d("ddddvvv", userId!!)
        viewModelScope.launch {
            sites = repository.getsiteDeatails(site, userId)

        }
   }

    fun getUserDetail(userId: String) {
        viewModelScope.launch {
            sites = repository.getDeatail(userId)
        }


    }
}
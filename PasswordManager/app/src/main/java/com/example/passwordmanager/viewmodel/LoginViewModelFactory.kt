package com.example.passwordmanager.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.passwordmanager.model.db.PasswordManagerDatabase
import com.example.passwordmanager.model.repositories.UserRepository
@Suppress("UNCHECKED_CAST")
class LoginViewModelFactory(
        private val repository: UserRepository,
        private val db: PasswordManagerDatabase

    ): ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return FragmentViewModel(repository, db) as T
        }
    }
